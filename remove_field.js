var bulk = db.AGGdata.initializeOrderedBulkOp();
var counter = 0;
db.AGGdata.find().forEach(function(doc) {
     var i = 39;
     while(i < 180) {
         var fld ='field' + i;
         db.AGGdata.update({}, { $unset: {fld:1} }, { multi: true });
         print("DSIntId:" + i);
         i = i + 1;
     }
     counter++;
     if (counter % 1000 == 0) {
         bulk.execute();
         bulk = db.AGGdata.initializeOrderedBulkOp();
     }
});
if (counter % 1000 != 0)
    bulk.execute();
