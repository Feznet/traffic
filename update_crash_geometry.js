var bulk = db.crashGeometry.initializeOrderedBulkOp();
var counter = 0;
db.crashGeometry.find().forEach(function(doc) {
     bulk.find({ "_id": doc._id }).updateOne({
        "$set": 
        {
            "location": 
            {
                "type": "Point",
                "coordinates" : [ doc.DOT_LONGIT, doc.DOT_LATITU ]
            }
        }});

     counter++;
     if ( counter % 1000 == 0 ) {
         bulk.execute();
         bulk = db.collection.initializeOrderedBulkOp();
     }
});

if ( counter % 1000 != 0)
    bulk.execute();
