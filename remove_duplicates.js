db.aggout13.aggregate(
    [ 
        //tweak by changing collection names
        // discard selection criteria, You can remove "$match" section if you want
        { $match : 
            { "DSIntId" : {"$ne" : ''} , "SampleTime" : {"$ne" : ''} }
        },
        { $group : 
            {
            _id: { stid : "$DSIntId", st : "SampleTime" }, // can be grouped on multiple properties
            dups: { "$addToSet" : "$_id" },
            count: { "$sum" : 1 }
            }
        },
        { $match: 
            {
            count: { "$gt" : 1 } // Duplicates considered as count greater than one
            }
        },
    ], 
    { allowDiskUse : true, cursor : {} }
).forEach(function(doc) {  // You can display result until this and check duplicates
    print(doc);
    doc.dups.shift();      // First element skipped for deleting
    db.collectionName.remove({ _id : { $in: doc.dups } });  // Delete remaining duplicates
});

var c = 0;
db.i35crashes.find({}).forEach(function(doc) {     
    var dir = doc.DIR;
    var lat = doc.DOT_LATITU;
    var long = doc.DOT_LONGIT;
    print(lat + " " + long);
    print(dir);
    stcurs=db.detectorstation.findOne( 
        {
            "DIR" : dir, "LocationReference" : { $not: /R/ },
            "location" : { $near : { $geometry : { type : "Point", coordinates : [long , lat] } } } 
        }
    );
    print(stcurs['IntId']);
    db.i35crashes.update(
        { "_id" : doc._id }, { $set : { DSIntId : stcurs['IntId'] } }
    );
    print(c);
    c = c + 1;
});
    
    