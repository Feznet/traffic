var bulk = db.agg13avg.initializeUnorderedBulkOp(), counter = 0;
var par = param;
print ("DSIntId : " + par);

//lane data
var ld1 = 0, ld2 = 0, ld3 = 0, ld4 = 0, ld5 = 0, ld6 = 0, ld7 = 0, ld8 = 0, ld9 = 0, ld10 = 0;

// lane data quality
var lq1 = 0, lq2 = 0, lq3 = 0, lq4 = 0, lq5 = 0, lq6 = 0, lq7 = 0, lq8 = 0, lq9 = 0, lq10 = 0;

// station datas
var sv = 0;
var sc = 0;
var sp = 0;
var so = 0;

//vehicle class
var vc1 = 0, vc2 = 0,vc3 = 0,vc4 = 0;

db.aggout13.find({"DSIntId" : par }).sort({"SampleTime" : 1}).noCursorTimeout()
  .forEach(function (doc) {
        var mins = doc.SampleTime.getMinutes();
        print("===============New Document==================");
        print("DSIntId : " + doc.DSIntId);
        print("Minutes : " + mins);
        print("Counter :" + counter);
        if (mins == 0 | mins == 15 | mins == 30 | mins == 45) {
            print("Category : " + 1); 
                if(doc.Station_Volume) sv = sv + doc.Station_Volume;
                if(doc.Station_Occupancy) so = so + doc.Station_Occupancy;
                if(doc.Station_Count) sc = sc + doc.Station_Count;
                if(doc.Station_Speed) sp = sp + doc.Station_Speed;

                if(doc.Lane1_Data) ld1 = ld1 + doc.Lane1_Data;
                if(doc.Lane2_Data) ld2 = ld2 + doc.Lane2_Data;
                if(doc.Lane3_Data) ld3 = ld3 + doc.Lane3_Data;
                if(doc.Lane4_Data) ld4 = ld4 + doc.Lane4_Data;
                if(doc.Lane5_Data) ld5 = ld5 + doc.Lane5_Data;
                if(doc.Lane6_Data) ld6 = ld6 + doc.Lane6_Data;
                if(doc.Lane7_Data) ld7 = ld7 + doc.Lane7_Data;
                if(doc.Lane8_Data) ld8 = ld8 + doc.Lane8_Data;
                if(doc.Lane9_Data) ld9 = ld9 + doc.Lane9_Data;
                if(doc.Lane10_Data) ld10 = ld10 + doc.Lane10_Data;

                if(doc.Lane1_DataQuality) lq1 = lq1 + doc.Lane1_DataQuality;
                if(doc.Lane2_DataQuality) lq2 = lq2 + doc.Lane2_DataQuality;
                if(doc.Lane3_DataQuality) lq3 = lq3 + doc.Lane3_DataQuality;
                if(doc.Lane4_DataQuality) lq4 = lq4 + doc.Lane4_DataQuality;
                if(doc.Lane5_DataQuality) lq5 = lq5 + doc.Lane5_DataQuality;
                if(doc.Lane6_DataQuality) lq6 = lq6 + doc.Lane6_DataQuality;
                if(doc.Lane7_DataQuality) lq7 = lq7 + doc.Lane7_DataQuality;
                if(doc.Lane8_DataQuality) lq8 = lq8 + doc.Lane8_DataQuality;
                if(doc.Lane9_DataQuality) lq9 = lq9 + doc.Lane9_DataQuality;
                if(doc.Lane10_DataQuality) lq10 = lq10 + doc.Lane10_DataQuality;

                if(doc.VehicleCountClass1) vc1 = vc1 + doc.VehicleCountClass1;
                if(doc.VehicleCountClass2) vc2 = vc2 + doc.VehicleCountClass2;
                if(doc.VehicleCountClass3) vc3 = vc3 + doc.VehicleCountClass3;
                if(doc.VehicleCountClass4) vc4 = vc4 + doc.VehicleCountClass4;

                sc = sc; //no average
                sp = sp / 3;
                sv = sv / 3;
                so = so / 3;
                vc1 = vc1 / 3;
                vc2 = vc2 / 3;
                vc3 = vc3 / 3; 
                vc4 = vc4 / 3;
                ld1 = ld1 / 3;
                ld2 = ld2 / 3;
                ld3 = ld3 / 3;
                ld4 = ld4 / 3;
                ld5 = ld5 / 3;
                ld6 = ld6 / 3;
                ld7 = ld7 / 3;
                ld8 = ld8 / 3;
                ld9 = ld9 / 3;
                ld10 = ld10 / 3;
                lq1 = lq1 / 3;
                lq2 = lq2 / 3;
                lq3 = lq3 / 3;
                lq4 = lq4 / 3;
                lq5 = lq5 / 3;
                lq6 = lq6 / 3;
                lq7 = lq7 / 3;
                lq8 = lq8 / 3;
                lq9 = lq9 / 3;
                lq10 = lq10 /3;

                print ("Sv1" + doc.Station_Volume + "calculated: " + sv);
                bulk.insert( {
                    "DSOrgId":doc.DSOrgId, "DSIntId":doc.DSIntId, "DSExtId":doc.DSExtId,"SampleTime":doc.SampleTime, "Station_Volume":sv,"SampleDuration":doc.SampleDuration, "Station_Count":sc,
                    "Station_Speed":sp,"Station_Status":doc.Station_Status, "Station_Volume_Completeness":doc.Station_Volume_Completeness, "Station_Speed_Completeness":doc.Station_Speed_Completeness,
                    "Station_Occupancy_Completeness":doc.Station_Occupancy_Completeness,"IsPeakHourAM":doc.IsPeakHourAM,"IsPeakHourPM":doc.IsPeakHourPM, "VehicleCountClass1":vc1, 
                    "VehicleCountClass2":vc2, "VehicleCountClass3":vc3,"VehicleCountClass4":vc4, "Station_Occupancy": so, "Lane1_Data": ld1,"Lane2_Data": ld2,"Lane3_Data": ld3,"Lane4_Data": ld4,
                    "Lane5_Data": ld5,"Lane6_Data": ld6,"Lane7_Data": ld7,"Lane8_Data": ld8, "Lane9_Data": ld9,"Lane10_Data": ld10, "Lane1_DataQuality": lq1, "Lane2_DataQuality": lq2,
                    "Lane3_DataQuality": lq3,"Lane4_DataQuality": lq4,"Lane5_DataQuality": lq5,"Lane6_DataQuality": lq6,"Lane7_DataQuality": lq7,"Lane8_DataQuality": lq8,"Lane9_DataQuality": lq9,
                    "Lane10_DataQuality": lq10 
                });

                print ("ID : " + doc._id);
                print("SampleTime : " + (doc.SampleTime).toGMTString());
                print("Document Inserted");
                counter++;
                if (counter % 100 == 0) {
                    bulk.execute();
                    bulk = db.agg13avg.initializeUnorderedBulkOp();
                    print("*********** Bulk Insert Complete ************");
                }

                sv = 0, sp = 0, sc = 0, so = 0;
                vc1 = 0, vc2 = 0, vc3 = 0, vc4 = 0;

                ld1 = 0, ld2 = 0, ld3 = 0, ld4 = 0, ld5 = 0, ld6 = 0, ld7 = 0, ld8 = 0, ld9 = 0, ld10 = 0;
                     
                lq1 = 0, lq2 = 0, lq3 = 0, lq4 = 0, lq5 = 0, lq6 = 0, lq7 = 0, lq8 = 0, lq9 = 0, lq10 = 0;
        }
        else if (mins == 5 | mins == 20 | mins == 35 | mins == 50) {
            print("Category : " + 2);
            if(doc.Station_Count) sc = sc + doc.Station_Count;
            if(doc.Station_Speed) sp = sp + doc.Station_Speed;
            if(doc.VehicleCountClass1) vc1 = vc1 + doc.VehicleCountClass1;
            if(doc.VehicleCountClass2) vc2 = vc2 + doc.VehicleCountClass2;
            if(doc.VehicleCountClass3) vc3 = vc3 + doc.VehicleCountClass3;
            if(doc.VehicleCountClass4) vc4 = vc4 + doc.VehicleCountClass4;

            if(doc.Station_Volume) sv = sv + doc.Station_Volume;
            if(doc.Station_Occupancy) so = so + doc.Station_Occupancy;
            if(doc.Lane1_Data) ld1 = ld1 + doc.Lane1_Data;
            if(doc.Lane2_Data) ld2 = ld2 + doc.Lane2_Data;
            if(doc.Lane3_Data) ld3 = ld3 + doc.Lane3_Data;
            if(doc.Lane4_Data) ld4 = ld4 + doc.Lane4_Data;
            if(doc.Lane5_Data) ld5 = ld5 + doc.Lane5_Data;
            if(doc.Lane6_Data) ld6 = ld6 + doc.Lane6_Data;
            if(doc.Lane7_Data) ld7 = ld7 + doc.Lane7_Data;
            if(doc.Lane8_Data) ld8 = ld8 + doc.Lane8_Data;
            if(doc.Lane9_Data) ld9 = ld9 + doc.Lane9_Data;
            if(doc.Lane10_Data) ld10 = ld10 + doc.Lane10_Data;

            if(doc.Lane1_DataQuality) lq1 = lq1 + doc.Lane1_DataQuality;
            if(doc.Lane2_DataQuality) lq2 = lq2 + doc.Lane2_DataQuality;
            if(doc.Lane3_DataQuality) lq3 = lq3 + doc.Lane3_DataQuality;
            if(doc.Lane4_DataQuality) lq4 = lq4 + doc.Lane4_DataQuality;
            if(doc.Lane5_DataQuality) lq5 = lq5 + doc.Lane5_DataQuality;
            if(doc.Lane6_DataQuality) lq6 = lq6 + doc.Lane6_DataQuality;
            if(doc.Lane7_DataQuality) lq7 = lq7 + doc.Lane7_DataQuality;
            if(doc.Lane8_DataQuality) lq8 = lq8 + doc.Lane8_DataQuality;
            if(doc.Lane9_DataQuality) lq9 = lq9 + doc.Lane9_DataQuality;
            if(doc.Lane10_DataQuality) lq10 = lq10 + doc.Lane10_DataQuality;
        }
        else if (mins == 10 | mins == 25 | mins == 40 | mins == 55) {
            print("Category : " + 3);
            if(doc.Station_Volume) sv = sv + doc.Station_Volume;
            if(doc.Station_Count) sc = sc + doc.Station_Count;
            if(doc.Station_Count) sp = sp + doc.Station_Speed;
            if(doc.Station_Occupancy) so = so + doc.Station_Occupancy;

            if(doc.VehicleClassCount1) vc1 = vc1 + doc.VehicleClassCount1;
            if(doc.VehicleClassCount2) vc2 = vc2 + doc.VehicleClassCount2;
            if(doc.VehicleClassCount3) vc3 = vc3 + doc.VehicleClassCount3;
            if(doc.VehicleClassCount4) vc4 = vc4 + doc.VehicleClassCount4;
                    
            if(doc.Lane1_Data) ld1 = ld1 + doc.Lane1_Data;
            if(doc.Lane2_Data) ld2 = ld2 + doc.Lane2_Data;
            if(doc.Lane3_Data) ld3 = ld3 + doc.Lane3_Data;
            if(doc.Lane4_Data) ld4 = ld4 + doc.Lane4_Data;
            if(doc.Lane5_Data) ld5 = ld5 + doc.Lane5_Data;
            if(doc.Lane6_Data) ld6 = ld6 + doc.Lane6_Data;
            if(doc.Lane7_Data) ld7 = ld7 + doc.Lane7_Data;
            if(doc.Lane8_Data) ld8 = ld8 + doc.Lane8_Data;
            if(doc.Lane9_Data) ld9 = ld9 + doc.Lane9_Data;
            if(doc.Lane10_Data) ld10 = ld10 + doc.Lane10_Data;

            if(doc.Lane1_DataQuality) lq1 = lq1 + doc.Lane1_DataQuality;
            if(doc.Lane2_DataQuality) lq2 = lq2 + doc.Lane2_DataQuality;
            if(doc.Lane3_DataQuality) lq3 = lq3 + doc.Lane3_DataQuality;
            if(doc.Lane4_DataQuality) lq4 = lq4 + doc.Lane4_DataQuality;
            if(doc.Lane5_DataQuality) lq5 = lq5 + doc.Lane5_DataQuality;
            if(doc.Lane6_DataQuality) lq6 = lq6 + doc.Lane6_DataQuality;
            if(doc.Lane7_DataQuality) lq7 = lq7 + doc.Lane7_DataQuality;
            if(doc.Lane8_DataQuality) lq8 = lq8 + doc.Lane8_DataQuality;
            if(doc.Lane9_DataQuality) lq9 = lq9 + doc.Lane9_DataQuality;
            if(doc.Lane10_DataQuality) lq10 = lq10 + doc.Lane10_DataQuality;
        }
        print("=======================");
    });

if(counter % 100 != 0) {
    bulk.execute();
}
print("-----------------DONE-----------------");