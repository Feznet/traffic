db.crashGeometry.find().limit(2).forEach(function(doc) {
    var long = doc.DOT_LONGIT;
    var lat = doc.DOT_LATITU;
    var dir = doc.DIR;
    var id = doc._id;
    print(long + " " + lat);
    var intid;
    try { 
       // filtering on direction as well as LocationReference to avoid ramps **caution : this will get stations on the highway even 
       // if the crashes occurred on the ramp
       var cursor = db.detectorstation.find({ 'DIR' : dir,"LocationReference" : { $not: /R/ } , "location" : { "$near" : { "$geometry" :
           { 'type' : "Point" , 'coordinates' : [ long , lat ] } } } }, 
           { 'IntId' : 1,'DOT_LONGIT' : 1, 'DOT_LATITU' : 1, 'Name' : 1,'DIR' : 1, 'LocationReference' : 1 }).limit(1);
       cursor.forEach(function(d) { 
           intid = d.IntId; 
           print(d.LocationReference);
       });
       print(intid);
       // db.crashGeometry.update({"_id":doc._id},{$set: { "DSIntId":intid} })
       // print(d.IntId + "Updated")
       print(doc._id + " " + doc.SampleTime + " ");
       // db.crashGeometry.update({_id:doc._id},{$set:{ 'DSIntId':st }   })
    }
    catch(err) {
       print(err);
    }
});
    