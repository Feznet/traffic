import sys
import pandas as pd
from pymongo import MongoClient
import datetime
import csv
import numpy as npedDict
from datetime import datetime

db = None

def connect():
	global db
	# Connect and get a client using a manually-made Mongo URI
	conn = MongoClient('mongodb://trafficteam:tuesday3pm0076@volare.kdd.cs.ksu.edu:7017/traffic')
	bigresult = pd.DataFrame()
	db = conn["traffic"]
	# start = datetime(2013, 1, 1, 0, 0, 0)
	# end = datetime(2016, 1, 1, 0, 0, 0)

	crashes = pd.DataFrame(list(db['i35crashes'].find( {}, {'location':0,'_id':0}) ) )
	# traffic = pd.DataFrame(list(db['AGGdata'].find({'SampleTime': {'$gte': start, '$lte': end}},{'location':0}) ))
	# weather = pd.DataFrame(list(db['KCWeather'].find({'SampleTime': {'$gte': start, '$lte': end}})) )
	# print(crashes)
	# Iterate through each of the rows fetched from the crashes
	for index, row in crashes.iterrows():
		rt = row['SampleTime']
		st = row['DSIntId']
		nm = row['ON_ROAD__1']
		dir = row['DIR']
		completedata = pd.Series()
		print("Timestamp : " + str(rt) + "Station ID : " + str(st) + "Direction : " + str(dir) + "Name of road: " + str(nm))
		crash = row
		completedata = completedata.append(crash)
		rt = pd.tslib.Timestamp(rt)
		starttime = rt - pd.Timedelta(minutes = 15)
		endtime = rt + pd.Timedelta(minutes = 30)
		# try to get data from the weather table for a specific time, if fails just create an empty dataframe
		try:
			weather = pd.DataFrame(list(db['KCWeather'].find({'SampleTime': {'$gte':starttime, '$lte':endtime} },
			{'SampleTime':0,'_id':0}).sort('SampleTime', 1)))
			# print(weather)
			for i, row in weather.iterrows():
				if i == 0:
					row.rename(lambda x: x + '-15', inplace=True)
				elif i == 1:
					row.rename(lambda x: x + ':0', inplace=True)
				elif i == 2:
					row.rename(lambda x: x + '+15', inplace=True)
				elif i == 3:
					row.rename(lambda x: x + '+30', inplace=True)
				completedata = completedata.append(row)
		except:
			weather = []
			completedata = completedata.append(weather)
			print(sys.exc_info())
			print('didnt work')
			# try to get data from the traffic table for a specific time and station id, if fails just create an empty dataframe
			try:
				traffic = pd.DataFrame(list(db['agg13avg'].find({'SampleTime': {'$gte': starttime, '$lte':
				endtime}, 'DIntId':st}, {'SampleTime': 0, '_id': 0}).sort('SampleTime', 1)))
				for i, row in traffic.iterrows():
					if i == 0:
						row.rename(lambda x: x + '-15', inplace=True)
					elif i == 1:
						row.rename(lambda x: x + ':0', inplace=True)
					elif i == 2:
						row.rename(lambda x: x + '+15', inplace=True)
					elif i == 3:
						row.rename(lambda x: x + '+30', inplace=True)
					completedata = completedata.append(row)
			except:
				traffic = []
				completedata = completedata.append(traffic)
				print(sys.exc_info())
			
			result = completedata.to_frame()
			result = result.T
			bigresult = bigresult.append(result)
			print('-----------------------------------------------')

		# write the complete result into a CSV file
		bigresult.to_csv('crashweather.csv', sep=',', encoding='utf-8')

connect()